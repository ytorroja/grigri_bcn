/* 
 Simple XBee AT API Example
 WS Node for GRIGRI PIXEL 2017 at Barcelona
 By: Yago Torroja
 v1.0 05/11/2017
 CC BY-SA 3.0
 */ 
 
#ifdef USE_NEOPIXEL
  #include <Adafruit_NeoPixel.h>
  #define LED_STRIP_PIN 8
  #define LED_STRIP_PIXELS 6
  Adafruit_NeoPixel strip = Adafruit_NeoPixel(LED_STRIP_PIXELS, LED_STRIP_PIN, NEO_GRB);
#else 
  #define R_PIN 9
  #define G_PIN 10
  #define B_PIN 11
#endif

uint32_t targetColor;  // Color to go to
uint32_t currentColor; // Current color
float currentR;
float currentG;
float currentB;

#define MY_COLOR 0xFF0000

///////////////////////////////////////////////////
// Setup function
void setupLedStrip() {
  #ifdef USE_NEOPIXEL
    strip.begin();  
  #else
  #endif
}

///////////////////////////////////////////////////
// Update function to include in loop.
// The function changes progresively to targetColor
void updateLedStrip(int wait) {
  gotoColor( targetColor, 0.01 );
  delay(wait);
}

///////////////////////////////////////////////////
// Set color of LED strip 
void setLedStripColor(uint32_t c) {
  #ifdef USE_NEOPIXEL
    for(uint16_t i = 0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, c);
    }  
    strip.show();
  #else  
    byte r = (c >> 16) & 0xFF;
    byte g = (c >>  8) & 0xFF;
    byte b = (c >>  0) & 0xFF;
    analogWrite(R_PIN, r);
    analogWrite(G_PIN, g);
    analogWrite(B_PIN, b);
  #endif
}

uint32_t getMyColor() {
  return myColor;
}

uint32_t getStandardColor(int n) {
  uint32_t r = MY_COLOR;
  switch ( n ) {
    case 0: r = 0x000000; break;
    case 1: r = 0xFF0000; break;
    case 2: r = 0x00FF00; break;
    case 3: r = 0x0000FF; break;
    case 4: r = 0xFFFFFF; break;      
  }
  return r;  
}

///////////////////////////////////////////////////
// Set color instantly 
void setColor(uint32_t c) {
  setLedStripColor(c);
  targetColor  = c;
  currentColor = targetColor;
}

///////////////////////////////////////////////////
// Goes linearly from current color to a certain
// color c interpolating at speed value (0.0 - 1.0)
void gotoColor(uint32_t c, float speed) {
  
  float rb = (c >> 16) & 0xFF;
  float gb = (c >>  8) & 0xFF;
  float bb = (c >>  0) & 0xFF;
  
  currentR = currentR + ((rb - currentR) * speed);
  currentG = currentG + ((gb - currentG) * speed);
  currentB = currentB + ((bb - currentB) * speed); 
   
  currentColor = getColorCode( currentR, currentG, currentB );
  setLedStripColor(currentColor);
}

// Goes linearly from current color to a certain
// color c interpolating at speed value (0.0 - 1.0)
void programColor(uint32_t c) {
  targetColor = c;
}

///////////////////////////////////////////////////
// Goes linearly from current color to a certain
// color c interpolating in ms milliseconds
void doColorTransition( uint32_t A, uint32_t B, int ms ) {

  float ra = (A >> 16) & 0xFF;
  float ga = (A >>  8) & 0xFF;
  float ba = (A >>  0) & 0xFF;

  float rb = (B >> 16) & 0xFF;
  float gb = (B >>  8) & 0xFF;
  float bb = (B >>  0) & 0xFF;
  
  for(int i = 0; i < ms; i++) {
    float val = (float)i / ms;
    float r = ra + ((rb - ra) * val);
    float g = ga + ((gb - ga) * val);
    float b = ba + ((bb - ba) * val);
    setLedStripColor( getColorCode(r, g, b) );
    delay(1);
  }
  
  currentColor = B;
  setLedStripColor(currentColor);
}

///////////////////////////////////////////////////
// Interpolates linearly between color A and B
// depending on val (val = 0.0 is all A, val = 1.0 is all B) 
uint32_t interpolateColor(uint32_t A, uint32_t B, float val) {  
  float ra = (A >> 16) & 0xFF;
  float ga = (A >>  8) & 0xFF;
  float ba = (A >>  0) & 0xFF;

  float rb = (B >> 16) & 0xFF;
  float gb = (B >>  8) & 0xFF;
  float bb = (B >>  0) & 0xFF;
  
  currentR = ra + ((rb - ra) * val);
  currentG = ga + ((gb - ga) * val);
  currentB = ba + ((bb - ba) * val);

  return getColorCode( currentR, currentG, currentB );
}

uint32_t getColorCode(byte r, byte g, byte b) {
  return ((uint32_t)r << 16) | ((uint32_t)g << 8) | ((uint32_t)b << 0);
}

///////////////////////////////////////////////////
// Returns a color based on a 0 to 255 value.
// The colours are a transition r - g - b - back to r
// From Adafruit NeoPixel library
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return getColorCode(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return getColorCode(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return getColorCode(WheelPos * 3, 255 - WheelPos * 3, 0);
}
