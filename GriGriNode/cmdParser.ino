/* 
 Simple XBee AT API Example
 WS Node for GRIGRI PIXEL 2017 at Barcelona
 By: Yago Torroja
 v1.0 05/11/2017
 CC BY-SA 3.0
 */ 
 
#define LED 13

SerialCommand sCmd;

String res;

void setupComm() {
  pinMode(LED, OUTPUT);
  // Input commands
  sCmd.addCommand("c", gotoStandardColor);
  sCmd.addCommand("h", gotoHexColor);
  sCmd.addCommand("y", beYourself);
  sCmd.addCommand("b", blinkStrip);
  sCmd.addCommand("p", setParameter);       // Set node parameters
  sCmd.addCommand("d", setDebugOutput);     // Set node debug printing
  // sCmd.addCommand("s", pauseInteraction);   
  sCmd.addCommand("r", resumeInteraction);   
  sCmd.addCommand("g", setGuardTime);       // st AT command interpreter guard time
  sCmd.addCommand("v", sendVersion);        // send version
  sCmd.addDefaultHandler(flashError);       // Error; flash quickly
}

void updateComm() {
    sCmd.readSerial();
}


void sendVersion() {
  Serial.print( F("v1.0 - ") );
  Serial.println( F(__DATE__) );
}

void setGuardTime() {
  char *arg; 
 
  res = "";
  arg = sCmd.next(); 
  if (arg != NULL)   {    
    int d = atoi(arg);
    if (d > 50) {
      if ( setGuardTime(d) ) {
        blinkStrip();
      }
    }
  }
}


// Set paramter:  p node command value
// node: node number (1-4) 0-all
// comand: t[ime] a[ccel] g[yro] s[ound] l[ight]
// value: integer
void setParameter() {
  char *arg; 
 
  res = "";
  arg = sCmd.next();  // Get node number 
  if (arg != NULL)   {
    int node = atoi(arg);
    if ((node == 0) || (node == getNodeNumber())) {
      arg = sCmd.next();  // get command
      if (arg != NULL)   {
        char command = arg[0];
        arg = sCmd.next();  // get value
        if (arg != NULL)   {
          int32_t val = atoi(arg);       
          switch (command) {
            case 't' : interactionTimeMs = val * 1000; break;
            case 'a' : accelTriggerLevel = val; break;
            case 'g' : gyroTriggerLevel  = val; break;
            case 's' : soundTriggerLevel = val; break;
            case 'l' : lightTriggerLevel = val; break;
            case 'c' : myColor = val; break;
          }
          saveParameters();
        }
      }
    }
  }
}

// Set debug action:  d node command
// node: node number (1-4) 0-all
// comand (print): a[ccel] g[yro] s[ound] l[ight] p[arameters]
void setDebugOutput() {
  char *arg; 

  doPrintAccelLevels = false;
  doPrintGyroLevels  = false;
  doPrintSoundLevel  = false;
  doPrintLightLevel  = false;

  pauseInteraction();
 
  res = "";
  arg = sCmd.next(); // get node number
  if (arg != NULL)   {
    int node = atoi(arg);
    if (node == getNodeNumber()) {
      arg = sCmd.next(); // get command
      if (arg != NULL)   {
        char command = arg[0];
        switch (command) {
          case 'a' : doPrintAccelLevels = true; break;
          case 'g' : doPrintGyroLevels  = true; break;
          case 's' : doPrintSoundLevel  = true; break;
          case 'l' : doPrintLightLevel  = true; break;
          case 'p' : printParameters(); break;
        }
      }
    }
  }
}

void flashError() {
  flashStrip(10, 50);
}

void blinkStrip() {
  flashStrip(2, 500);
}

void flashStrip(int n, int d) {
  for( int i = 0; i < (n << 1); i++) {
    setColor( getMyColor() );
    delay(d);
    setColor( 0 );
    delay(d);
  }
}

void pauseInteraction() {
  isPaused = true;
  resumeTimeMillis = millis() + 30000;
}

void resumeInteraction() {
  doPrintAccelLevels = false;
  doPrintGyroLevels  = false;
  doPrintSoundLevel  = false;
  doPrintLightLevel  = false;

  isPaused = false;  
}

void gotoStandardColor() {
  char *arg; 

  arg = sCmd.next(); 
  if (arg != NULL)   {
    int c = atoi(arg);
    setStandardTargetColor(c);
  }
}

void gotoHexColor() {
  char *arg; 

  arg = sCmd.next(); 
  if (arg != NULL)   {
    uint32_t c = strtol(arg, NULL, 16);
    programColor( c );
  }
}

void beYourself() {
  programColor( getMyColor() );
  stopTimeMillis = millis() + interactionTimeMs;
}

void setStandardTargetColor(byte d) {
  switch (d) {
    case 0 : targetColor = 0x000000; break;
    default: targetColor = getStandardColor(d); stopTimeMillis = millis() + interactionTimeMs; break;
  }
}

