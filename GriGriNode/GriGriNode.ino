/* 
 Simple XBee AT API Example
 WS Node for GRIGRI PIXEL 2017 at Barcelona
 By: Yago Torroja
 v1.0 05/11/2017
 CC BY-SA 3.0
 */ 

/////////////////////////////////////////////////////
// Configuration section (modify as desired)
// Kind of led strip
//#define USE_NEOPIXEL

// Communication Speed
#define BAUD_RATE        9600

#define STEP_MILLISECONDS  10

#define INTERACTION_TIME_MILLISECONDS 5000

#define ACCEL_TRIGGER_LEVEL 100

#define GYRO_TRIGGER_LEVEL 200

#define SOUND_TRIGGER_LEVEL 500

#define LIGHT_TRIGGER_LEVEL 200

/////////////////////////////////////////////////////
// Implementation section (should not be modified ;)
#include <Wire.h>
#include <SerialCommand.h>

// Software version for EEPROM check
#define CURRENT_VERSION 1

/////////////// 
// Derived and internal defines
#define LED 13
#define X 0
#define Y 1
#define Z 2
#define V 3

uint32_t stopTimeMillis;
uint32_t resumeTimeMillis;
boolean waitingToStop;
boolean isPaused;

int stepMilliseconds;
int interactionTimeMs;
int accelTriggerLevel;
int gyroTriggerLevel;
int soundTriggerLevel;
int lightTriggerLevel;

boolean doPrintAccelLevels;
boolean doPrintGyroLevels;
boolean doPrintSoundLevel;
boolean doPrintLightLevel;

uint32_t myColor;


///////////////
// Node setup
void setup() {

  Serial.begin(BAUD_RATE); 

  Wire.begin();

  setupSound();

  setupAccelGyro();

  setupLedStrip();

  setupXBee();

  setupComm();

  myColor = getStandardColor( getNodeNumber() );
  stepMilliseconds  = STEP_MILLISECONDS;
  interactionTimeMs = INTERACTION_TIME_MILLISECONDS;
  accelTriggerLevel = ACCEL_TRIGGER_LEVEL;
  gyroTriggerLevel  = GYRO_TRIGGER_LEVEL;
  soundTriggerLevel = SOUND_TRIGGER_LEVEL;
  lightTriggerLevel = LIGHT_TRIGGER_LEVEL;
  
  loadParameters();
  
  isPaused = false;
}


// Main loop
void loop() {

  // Update Accel & Gyro sensor data
  updateAccelGyro();
  // Update sound sensor data
  updateSound();
  // Update light sensor data
  updateLight();
  // Update led strip
  updateLedStrip( stepMilliseconds );
  // Process input commands if available
  updateComm();

  // Si estamos pausados podemos printar cosas
  if (isPaused) {
    if ( doPrintAccelLevels ) printAccelLevels();    
    if ( doPrintGyroLevels ) printGyroLevels();    
    if ( doPrintSoundLevel ) printSoundLevel();
    if ( doPrintLightLevel ) printLightLevel(); 
    if ( millis() > resumeTimeMillis ) resumeInteraction();
    return;       
  }

  // Si hay demasiada luz, no hacemos nada, porque no se ve
  if ( getLightLevel() > 200 ) {
    // Should sleep for a while
    return;  
  }
  
  // Si se detecta golpe (alguien se sienta) se pone de su color durante 2 o tres segundos
  if ( ( abs( getAccelLevel(V) - getAccelMeanV() ) > 100 ) && !waitingToStop ) {
    beYourself();
    stopTimeMillis = millis() + interactionTimeMs;
    waitingToStop = true;
  }

  // Cuando uno se mueve todos se ponen del color del que se mueve hasta que pare
  if ( ( abs( getGyroLevel(V) - getGyroMeanV() ) > 200 ) && !waitingToStop ) {
    beYourself();
    // Serial.println( "c " + getNodeName() );
    Serial.print( "h ");
    Serial.println( getMyColor(), HEX );
    stopTimeMillis = millis() + interactionTimeMs;
    waitingToStop = true;
  }

  // Si hay mucho ruido nos apagamos y se lo decimos a los demas
  if ( getSoundLevel() > 500 && !waitingToStop) {
    programColor(0);
    Serial.println("c 0");
    waitingToStop = true;
    stopTimeMillis = millis() + interactionTimeMs;
  }

  // Cuando pasas el tiempo suficiente nos apagamos
  if ( waitingToStop && (millis() > stopTimeMillis) ) {
    programColor(0);
    Serial.println("c 0");
    waitingToStop = false;
  }
   
}




