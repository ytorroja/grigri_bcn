#include <EEPROM.h>

void saveParameters() {
  int idx = 0;
  byte ver = CURRENT_VERSION;
  EEPROM.put( idx, CURRENT_VERSION ); idx += sizeof( ver ); 
  EEPROM.put( idx, myColor ); idx += sizeof( myColor ); 
  EEPROM.put( idx, interactionTimeMs ); idx += sizeof( interactionTimeMs ); 
  EEPROM.put( idx, accelTriggerLevel ); idx += sizeof( accelTriggerLevel ); 
  EEPROM.put( idx, gyroTriggerLevel ); idx += sizeof( gyroTriggerLevel ); 
  EEPROM.put( idx, soundTriggerLevel ); idx += sizeof( soundTriggerLevel ); 
  EEPROM.put( idx, lightTriggerLevel ); idx += sizeof( lightTriggerLevel ); 
}

boolean loadParameters() {
  int idx = 0;
  byte ver;
  EEPROM.get( idx, ver ); idx += sizeof( ver ); 
  if ( ver == CURRENT_VERSION ) {
    EEPROM.get( idx, myColor ); idx += sizeof( myColor ); 
    EEPROM.get( idx, interactionTimeMs ); idx += sizeof( interactionTimeMs ); 
    EEPROM.get( idx, accelTriggerLevel ); idx += sizeof( accelTriggerLevel ); 
    EEPROM.get( idx, gyroTriggerLevel ); idx += sizeof( gyroTriggerLevel ); 
    EEPROM.get( idx, soundTriggerLevel ); idx += sizeof( soundTriggerLevel ); 
    EEPROM.get( idx, lightTriggerLevel ); idx += sizeof( lightTriggerLevel ); 
  }
  return ver;
}

void printParameters() {
  Serial.print("N:" + getNodeId());
  Serial.print("  C:");
  Serial.print(myColor, HEX);
  Serial.print("  T:");
  Serial.print(interactionTimeMs);
  Serial.print("  A:");
  Serial.print(accelTriggerLevel);
  Serial.print("  G:");
  Serial.print(gyroTriggerLevel);
  Serial.print("  S:");
  Serial.print(soundTriggerLevel);
  Serial.print("  L:");
  Serial.print(lightTriggerLevel);
  Serial.println();
}

