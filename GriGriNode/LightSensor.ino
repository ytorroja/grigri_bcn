/* 
 Simple XBee AT API Example
 WS Node for GRIGRI PIXEL 2017 at Barcelona
 By: Yago Torroja
 v1.0 05/11/2017
 CC BY-SA 3.0
 */ 

#define LIGHT_ADC_CHANNEL A1
#define LIGHT_FIR_TAPS 32

uint16_t lightFir[LIGHT_FIR_TAPS];
uint16_t lightFirIdx;
uint32_t lightFirAccum;

void setupLight() {
  
}

void updateLight() {
  lightFirAccum -= lightFir[lightFirIdx];
  lightFir[lightFirIdx] = analogRead(LIGHT_ADC_CHANNEL);
  lightFirAccum += lightFir[lightFirIdx];
  lightFirIdx = (lightFirIdx + 1) % LIGHT_FIR_TAPS;
}

int getLightLevel() {
  return lightFirAccum / LIGHT_FIR_TAPS;
}


int getLight() {
  return analogRead(LIGHT_ADC_CHANNEL);
}

void printLightLevel() {
  Serial.println(getLightLevel());
}

void onLight(int level, int (* func)(int l)) {
   
}

