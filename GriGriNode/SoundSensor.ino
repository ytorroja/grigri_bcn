/* 
 Simple XBee AT API Example
 WS Node for GRIGRI PIXEL 2017 at Barcelona
 By: Yago Torroja
 v1.0 05/11/2017
 CC BY-SA 3.0
 */ 
 
#define SOUND_ADC_CHANNEL A2
#define SOUND_FIR_TAPS 32

uint16_t soundFir[SOUND_FIR_TAPS];
uint16_t soundFirIdx;
uint32_t soundFirAccum;

int soundMean;

void setupSound() {
  uint32_t acc = 0;
  for(int i = 0; i < 10000; i++) {
    acc += analogRead(SOUND_ADC_CHANNEL);
  }
  soundMean = acc / 10000;
}

void updateSound() {
  soundFirAccum -= soundFir[soundFirIdx];
  uint16_t absVal = abs(analogRead(SOUND_ADC_CHANNEL) - soundMean);
  soundFir[soundFirIdx] = absVal;
  soundFirAccum += soundFir[soundFirIdx];
  soundFirIdx = (soundFirIdx + 1) % SOUND_FIR_TAPS;
}

int getSoundLevel() {
  return soundFirAccum / SOUND_FIR_TAPS;
}

int getSound() {
  return analogRead(SOUND_ADC_CHANNEL);
}

void printSoundLevel() {
  Serial.println(getSoundLevel());
}

void onSound(int level, int (* func)(int l)) {
   
}

