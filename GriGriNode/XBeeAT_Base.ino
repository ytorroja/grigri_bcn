/* 
Simple XBee AT API Example
WS Node for GRIGRI PIXEL 2017 at Barcelona
By: Yago Torroja
v1.0 05/11/2017
CC BY-SA 3.0
*/
 
#include <String.h>


#define TIMEOUT     4000
#define DEBUG_COMM  false

String CR_STR   = "\r";
String OK_STR   = "OK\r";
String NULL_STR = "";

String   nodeId = "1";

String  str_buffer;
int     timeout    = TIMEOUT;
boolean debug_comm = DEBUG_COMM;

boolean autoCommand  = true;
int     guardTime    = 1024;

void setupXBee() {
  // Wait for the XBee node to wake up
  delay(3000);

  // Reduce guard time to spped up communication with XB module
  setGuardTime(100);

  // Set broadcast
  sendATCommandWithParameter("ATDH", "0");
  sendATCommandWithParameter("ATDL", "FFFF");
  
  // Say hello
  nodeId = getNodeId();
  nodeId = getNodeId();
  Serial.print( F("\nXBee Node ") );
  Serial.println( nodeId );
}


String getNodeName() {
  return nodeId;
}

int getNodeNumber() {
  return atoi(nodeId.c_str());
}

String getResponse() {
  return str_buffer;
}

void setAutoCmdMode(boolean b) {
  autoCommand = b;
}

boolean setDestinationNodeByNumber(int n) {
  boolean res = false;
  String  hex = String(n, HEX);
  timeout = 5 * TIMEOUT;
  setAutoCmdMode(false);
  if (getIntoCmdMode()) res = sendATCommandWithParameter("ATDN", hex);
  timeout = TIMEOUT;
  setAutoCmdMode(true);
  return res;
}

String getNodeId() {
  return getResponseFromATCommand("ATMY");
}

//String getNodeName() {
//  return getResponseFromATCommand("ATNI");
//}

boolean setGuardTime(int d) {
  String hex = String(d, HEX);
  if ( sendATCommandWithParameter("ATGT", hex) ) {
    guardTime = d + 4;
    return true;
  }
  return false;
}

String getNodeSerialLow() {
  return getResponseFromATCommand("ATSL");
}

String getNodeSerialHigh() {
  return getResponseFromATCommand("ATSH");
}

boolean getIntoCmdMode() {
  // Guard times and command entry
  delay(guardTime);
  Serial.write("+++");
  delay(guardTime);    
  // Wait for response for TIMEOUT milliseconds
  if ( waitForResponse(OK_STR, timeout) ) return true;
  // Failed to get response in time
  return false;
}

boolean exitFromCmdMode() {
  if ( writeCommand("ATCN\r", OK_STR, timeout) ) return true;
  return false;
}

boolean sendATCommand(String cmd) {
  boolean inCmd = false;
  if (autoCommand) {
    inCmd = getIntoCmdMode();
  } else {
    inCmd = true;
  }
  if (inCmd) {
    writeCommandWithParameter(cmd, CR_STR, NULL_STR, timeout);
    str_buffer = "";
    if (readResponseUntil(CR_STR, timeout)) {
      // ATTN: Command should return OK!
      if (str_buffer.indexOf(OK_STR) != -1) {
        if (autoCommand) exitFromCmdMode();  // Get out of command mode
        return true;
      }         
    }
    if (autoCommand) exitFromCmdMode();  // Get out of command mode
  }   
  // failed to get in command mode or get data... return false
  return false;
}

boolean sendATCommandWithParameter(String cmd, String par) {
  boolean inCmd = false;
  if (autoCommand) {
    inCmd = getIntoCmdMode();
  } else {
    inCmd = true;
  }
  if (inCmd) {
    writeCommandWithParameter(cmd, par, NULL_STR, timeout);
    str_buffer = "";
    if (readResponseUntil(CR_STR, timeout)) {
      // ATTN: Command should return OK!
      if (str_buffer.indexOf(OK_STR) != -1) {
        if (autoCommand) exitFromCmdMode();  // Get out of command mode
        return true; 
      }         
    }
    if (autoCommand) exitFromCmdMode();  // Get out of command mode
  }   
  // failed to get in command mode or get data... return false
  return false;
}

String getResponseFromATCommand(String cmd) {
  boolean inCmd = false;
  if (autoCommand) {
    inCmd = getIntoCmdMode();
  } else {
    inCmd = true;
  }
  if (inCmd) {
    writeCommandWithParameter(cmd, CR_STR, NULL_STR, timeout);
    str_buffer = "";
    if (readResponseUntil(CR_STR, timeout)) {
      if (autoCommand) exitFromCmdMode();  // Get out of command mode
      str_buffer.trim();
      return str_buffer;
    }
    if (autoCommand) exitFromCmdMode();  // Get out of command mode
  }   
  // failed to get in command mode or get data... return no answer
  str_buffer = ""; 
  return str_buffer;
}

boolean writeCommand(String cmd, String res, int timeOut) {
  Serial.print(cmd);
  return waitForResponse(res, timeOut);
}

boolean writeCommandWithParameter(String cmd, String par, String res, int timeOut) {
  // Generate command
  String command  = "";
  command += cmd;
  command += par;
  command += CR_STR;  
  Serial.print(command);
  return waitForResponse(res, timeOut);
}

boolean waitForResponse(String str, int timeOut) {
  long tOut = millis() + timeOut;
  int cIdx = 0;
  //println("... waitForResponse");
  while ( millis() < tOut && cIdx < str.length()) { // str.charAt(cIdx) != '\0' ) {
    if ( Serial.available() > 0 ) {
      char cIn = (char)Serial.read();
      if ( debug_comm ) Serial.print(cIn);
      if ( cIn == str.charAt(cIdx) ) {
        cIdx++;
      } 
      else {
        cIdx = 0;
      }
    }
  }
  //println("...");
  if ( cIdx == str.length() ) return true; // str.charAt(cIdx) == '\0') return true;
  return false;
}

boolean readResponseUntil(String until, int timeOut) {
  long tOut = millis() + timeOut;
  int sIdx = 0;
  int uIdx = 0;
  str_buffer = "";
  //println("---resResponseUntil");
  while ( millis() < tOut && uIdx < until.length()) { // until.charAt(uIdx) != '\0' ) {
    if ( Serial.available() > 0 ) {
      char cIn = (char)Serial.read();
      if ( debug_comm ) Serial.print(cIn);
      str_buffer += cIn; // str_buffer[sIdx++] = cIn;
      if ( cIn == until.charAt(uIdx) ) {
        uIdx++;
      } 
      else {
        uIdx = 0;
      }
    }
  }
  // println("---");
  if (uIdx == until.length()) { // until.charAt(uIdx) == '\0') {
    // Put terminator string out of response
    // str_buffer += '\0'; //str_buffer[sIdx - uIdx] = '\0';
    return true;
  }
  // We did not find terminator, so restore all
  str_buffer = ""; 
  return false;
} 



