/* 
 Simple XBee AT API Example
 WS Node for GRIGRI PIXEL 2017 at Barcelona
 By: Yago Torroja
 v1.0 05/11/2017
 CC BY-SA 3.0
 */ 
 
#include "SparkFunLSM6DS3.h"
#include "Wire.h"
#include "SPI.h"

LSM6DS3 myIMU; //Default constructor is I2C, addr 0x6B

#define  AG_FIR_TAPS 8
int16_t accelFir[3][AG_FIR_TAPS];
int16_t gyroFir[3][AG_FIR_TAPS];
int32_t accelFirAccum[3];
int32_t gyroFirAccum[3];
int16_t agFirIdx;

int accelMeanV;
int gyroMeanV;

void setupAccelGyro() {
  myIMU.begin();  
  int32_t acc = 0;
  for(int i = 0; i < 1000; i++) {
    acc += getAccel(V);
  }
  accelMeanV = acc / 1000;
  acc = 0;  
  for(int i = 0; i < 1000; i++) {
    acc += getGyro(V);
  }
  gyroMeanV = acc / 1000;  
}

int getAccel(int v) {
  int r = 0;
  switch (v) {
    case X: r = myIMU.readRawAccelX(); break;
    case Y: r = myIMU.readRawAccelY(); break;
    case Z: r = myIMU.readRawAccelZ(); break;
    case V: {
      int32_t v = 0;
      r = myIMU.readRawAccelX(); 
      v += sq((int32_t)r);
      r = myIMU.readRawAccelY(); 
      v += sq((int32_t)r);
      r = myIMU.readRawAccelZ(); 
      v += sq((int32_t)r);
      v = sqrt(v);
      r = v;
      break;
    }
  }
  return r;
}

int getGyro(int v) {
  int r = 0;
  switch (v) {
    case X: r = myIMU.readRawGyroX(); break;
    case Y: r = myIMU.readRawGyroY(); break;
    case Z: r = myIMU.readRawGyroZ(); break;
    case V: {
      int32_t v = 0;
      r = myIMU.readRawGyroX(); 
      v += sq((int32_t)r);
      r = myIMU.readRawGyroY(); 
      v += sq((int32_t)r);
      r = myIMU.readRawGyroZ(); 
      v += sq((int32_t)r);
      v = sqrt(v);
      r = v;
      break;
    }
  }
  return r;  
}

float getTemp() {
  myIMU.readTempC();
}

int getAccelMeanV() {
  return accelMeanV;
}

int getGyroMeanV() {
  return gyroMeanV;
}

void updateAccelGyro() {
  for(int i = 0; i < 3; i++) {
    accelFirAccum[i] -= accelFir[i][agFirIdx];
    accelFir[i][agFirIdx] = getAccel(i);
    accelFirAccum[i] += accelFir[i][agFirIdx];

    gyroFirAccum[i] -= gyroFir[i][agFirIdx];
    gyroFir[i][agFirIdx] = getGyro(i);
    gyroFirAccum[i] += gyroFir[i][agFirIdx];

    agFirIdx = (agFirIdx + 1) % AG_FIR_TAPS;
  }
}

int getAccelLevel(int v) {
  int r = 0;
  switch (v) {
    case X: r = accelFirAccum[X] / AG_FIR_TAPS; break;
    case Y: r = accelFirAccum[Y] / AG_FIR_TAPS; break;
    case Z: r = accelFirAccum[Z] / AG_FIR_TAPS; break;
    case V: {
      int32_t v = 0;
      r = accelFirAccum[X] / AG_FIR_TAPS; 
      v += sq((int32_t)r);
      r = accelFirAccum[Y] / AG_FIR_TAPS; 
      v += sq((int32_t)r);
      r = accelFirAccum[Z] / AG_FIR_TAPS; 
      v += sq((int32_t)r);
      v = sqrt(v);
      r = v;
      break;
    }
  }
  return r;  
}

int getGyroLevel(int v) {
  int r = 0;
  switch (v) {
    case X: r = gyroFirAccum[X] / AG_FIR_TAPS; break;
    case Y: r = gyroFirAccum[Y] / AG_FIR_TAPS; break;
    case Z: r = gyroFirAccum[Z] / AG_FIR_TAPS; break;
    case V: {
      int32_t v = 0;
      r = gyroFirAccum[X] / AG_FIR_TAPS; 
      v += sq((int32_t)r);
      r = gyroFirAccum[Y] / AG_FIR_TAPS; 
      v += sq((int32_t)r);
      r = gyroFirAccum[Z] / AG_FIR_TAPS; 
      v += sq((int32_t)r);
      v = sqrt(v);
      r = v;
      break;
    }
  }
  return r; }

void printAccels() {
  Serial.print(getAccel(X));
  Serial.print("\t");
  Serial.print(getAccel(Y));
  Serial.print("\t");
  Serial.print(getAccel(Z));
  Serial.print("\n");
}

void printGyros() {
  Serial.print(getGyro(X));
  Serial.print("\t");
  Serial.print(getGyro(Y));
  Serial.print("\t");
  Serial.print(getGyro(Z));
  Serial.print("\n");  
}

void printAccelLevels() {
  Serial.print(getAccelLevel(X));
  Serial.print("\t");
  Serial.print(getAccelLevel(Y));
  Serial.print("\t");
  Serial.print(getAccelLevel(Z));
  Serial.print("\n");
}

void printGyroLevels() {
  Serial.print(getGyroLevel(X));
  Serial.print("\t");
  Serial.print(getGyroLevel(Y));
  Serial.print("\t");
  Serial.print(getGyroLevel(Z));
  Serial.print("\n");  
}

void onAccel(int level, int (* func)(int l)) {
   
}

void onGyro(int level, int (* func)(int l)) {
   
}

